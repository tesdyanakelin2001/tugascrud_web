<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Data Mahasiswa | Tambah Data</title>
    <link rel="stylesheet" href="/css/bootstrap.min.css">
</head>
<body>
    <nav class="container mt-4 bg-primary text-left">
        <h3>TAMBAH DATA MAHASISWA</h3>
    </nav>
    <div class="container">
        <form action="/insert" method="post">
            {{csrf_field()}}
            <div class="form-group">
                <label for="nama">Nama</label>
                <input type="text" class="form-control" id="nama" name="nama" placeholder="Masukkan nama mahasiswa" autocomplete="off" autofocus>
            </div>
            <div class="form-group">
                <label for="nim">NIM</label>
                <input type="number" class="form-control" id="nim" name="nim" placeholder="Masukkan nim mahasiswa" autocomplete="off">
            </div>
            <div class="form-group">
                <label for="kelas">kelas</label>
                <input type="text" class="form-control" id="kelas" name="kelas" placeholder="Masukkan kelas mahasiswa">
            </div>
            <div class="form-group">
                <label for="prodi">Prodi</label>
                <input type="text" class="form-control" id="prodi" name="prodi" placeholder="Masukkan prodi mahasiswa">
            </div>
            <div class="form-group">
                <label for="fakultas">Fakultas</label>
                <input type="text" class="form-control" id="fakultas" name="fakultas" placeholder="Masukkan fakultas mahasiswa">
            </div>
            <button type="submit" class="btn btn-success float-right">Tambah</button>
        </form>
    </div>
</body>
</html>