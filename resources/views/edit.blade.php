<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Data Mahasiswa | Edit Data</title>
    <link rel="stylesheet" href="/css/bootstrap.min.css">
</head>
<body>
    <nav class="container mt-5 bg-red text-dark text-center">
        <h3>EDIT DATA MAHASISWA</h3>
    </nav>

    <div class="container mt-4">
        <a href="/"><button type="button" class="btn btn-warning">Kembali</button></a>
    </div>

    <div class="container">
        @foreach($mahasiswa as $m)
            <form action="/update" method="post">
                {{csrf_field()}}
                <div class="form-group">
                    <input type="hidden" class="form-control" name="id" value="{{$m->id}}"></input>
                </div>
                <div class="form-group">
                    <label for="nama">Nama</label>
                    <input type="text" class="form-control" id="nama" name="nama" placeholder="Masukkan nama mahasiswa " autocomplete="off" autofocus value="{{$m->nama_mahasiswa}}">
                </div>
                <div class="form-group">
                    <label for="nim">NIM</label>
                    <input type="number" class="form-control" id="nim" name="nim" placeholder="Masukkan nim mahasiswa" autocomplete="off" value="{{$m->nim_mahasiswa}}">
                </div>
                <div class="form-group">
                    <label for="kelas">Kelas</label>
                    <input type="text" class="form-control" id="kelas" name="kelas" placeholder="Masukkan kelas mahasiswa" value="{{$m->kelas_mahasiswa}}">
                </div>
                <div class="form-group">
                    <label for="prodi">Prodi</label>
                    <input type="text" class="form-control" id="prodi" name="prodi" placeholder="Masukkan prodi mahasiswa" value="{{$m->prodi_mahasiswa}}">
                </div>
                <div class="form-group">
                    <label for="fakultas">Fakultas</label>
                    <input type="text" class="form-control" id="fakultas" name="fakultas" placeholder="Masukkan fakultas mahasiswa" value="{{$m->fakultas_mahasiswa}}">
                </div>
                <button type="submit" class="btn btn-primary float-right">Simpan</button>
            </form>
        @endforeach
    </div>
</body>
</html>